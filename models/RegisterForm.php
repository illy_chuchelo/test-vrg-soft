<?php

namespace app\models;

use yii\base\Model;
use Yii;


class RegisterForm extends Model
{
  public $first_name;
  public $last_name;
  public $phone;
  public $address;
  public $comment;
  public $feetbackDataId;

  public function rules()
  {
    return [
      [['first_name', 'last_name', 'phone', 'address', 'comment'], 'required'],
      [['first_name', 'last_name', 'address', 'comment'],  'string'],
      ['phone', 'number']
    ];
  }

  public function register()
  {
      if($this->validate())
      {
          $user = new User();

          $user->attributes = $this->attributes;
          $user->feetbackDataId = md5(uniqid(rand(),1));
          Yii::$app->session->setFlash('success', 'Ваш аккаунт был зарегистрирован '.$user->feetbackDataId);

          return $user->create();
      }
  }

}
