<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\RegisterForm;
use yii\web\Cookie;
use Yii;


class AuthController extends Controller
{

  public function actionRegister()
  {

    $model = new RegisterForm();

    if (Yii::$app->request->isPost)
    {
      $model->load(Yii::$app->request->post());

          if($model->register())
          {
            return $this->redirect(Yii::$app->homeUrl);
          }
    }

    return $this->render('register', ['model' => $model]);
  }

}
