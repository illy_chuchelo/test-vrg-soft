<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->registerJsFile('/js/script1.js');
$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Register</h1>
<!-- почему тут работает по другому -->
  <ul class="nav nav-tabs" style="padding-top: 10px;">
    <li class="active"><a href="#program-selection-step-1" data-toggle="tab"><i>Имя, Фамилия, Номер телефона</i></a></li>
    <li><a href="#program-selection-step-2" data-toggle="tab"><i>Адрес</i></a></li>
    <li><a href="#program-selection-step-3" data-toggle="tab"> <i>Комментари</i></a></li>
  </ul>

<div class="program-selection" style="padding-top: 20px;">

      <div class="row">
          <div class="col-lg-5">
            <?php $form = ActiveForm::begin([ 'id' => 'register-form' ]); ?>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="program-selection-step-1">

                        <?= $form->field($model, 'first_name')->textInput( ['placeholder' => 'John', 'class' => 'form-control ntSaveForms'] ) ?>
                        <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Doe', 'class' => 'form-control ntSaveForms']) ?>
                        <?= $form->field($model, 'phone')->textInput( ['placeholder' => '+38050123456', 'class' => 'form-control ntSaveForms']) ?>

                    </div><!-- tab-pane -->

                    <div class="tab-pane fade in" id="program-selection-step-2">
                        <?= $form->field($model, 'address')->textInput(['placeholder' => 'Dnipro, Naberezhnaya st. 22', 'class' => 'form-control ntSaveForms']) ?>
                      </div><!-- tab-pane -->

                    <div class="tab-pane fade in" id="program-selection-step-3">
                        <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'class' => 'form-control ntSaveForms']) ?>
                        <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary ntSaveFormsSubmit', 'name' => 'register-button']) ?>
                    </div><!-- tab-pane -->

                </div><!-- tab-content -->
            <?php ActiveForm::end(); ?>
          </div>
        </div>

    </div><!-- program-selection -->
