
$(function(){

	/* Inits */
	initProgramSelection();

  /* Functions */
  function initProgramSelection(){
  	var $program_selection = $('div.program-selection');

    $('ul.nav-tabs a', $program_selection).on('click', function() {
      return false;
    });

    $('input.program-selection__button', $program_selection).on('click', function(e) {
    	e.preventDefault();

      var id = $(this).data('href');
      /* Далее нужно добавить проверку валидности заполненных полей перед переходом на следующий шаг */
      $('a[href="' + id + '"]', $program_selection).tab('show');
    });

      $("#tabs").tabs({
          active: localStorage.getItem("currentTabIndex"),
          activate: function(event, ui) {
              localStorage.setItem("currentTabIndex", ui.newPanel[0].dataset["tabIndex"]);
          }
      });

  }


});
